define(function () {
	
	return function makePopup(data) {     
		var needsForm = false;
		var events = {};
		var html = data.map(function(x){
			if (x.hasOwnProperty('label')) {
				return 	`${x.label.label}`;
			}
			if (x.hasOwnProperty('text')) {
				needsForm = true;				
				return 	`<label>${x.text.label}</label><input type="text" name="${x.text.name}">`;
			}			
			if (x.hasOwnProperty('button')) {
				//guid generation to bind function to element events
				var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
					var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
					return v.toString(16);
				});
				
				events[guid] = x.button.click;
				
				return `<button id="${guid}">${x.button.label}</button>`;
			}
			if (x.hasOwnProperty('close')) {
				return 	`<button class="close">Закрыть</button>`;
			}
			return "";
		})
		.filter(function(x){
			return x != "";
		})
		.reduce(function(concat, current) {
			var template = `
				<p>${current}</p>
			`			
			return concat + template;
		}, "");
		
		if (needsForm) {html = `<form>${html}</form>`};
		return [html,events];
    };
});