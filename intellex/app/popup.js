define(function (require) {
	var $ = require("jquery");	
	
	var popupTemplate =`				
		<div class="popup-back">
			<div class="popup-content-box"></div>
		</div>
	`;	
	
	var formData ={};
	var hide = function () {		
			console.log("hide");			
				$(".popup-content-box form")				
				.serializeArray()
				.forEach(function (x){					
					formData[x.name]=x.value;
				},{});								
				console.log(JSON.stringify(formData));
				$( ".popup-back" ).remove();			
        };		
	
	return {
        show: function () {            
			//removes all other popups
			hide();
							
			$( "body" ).append(popupTemplate);
			
			$(document).on('keydown',function(e) {
				// ESCAPE key pressed
				if (e.keyCode == 27) {						
					$(this).off(e);
					hide();
				}
			});
			
			// close button event
			$('body').on('click','.popup-content-box .close', function(e) {
				hide();
				e.preventDefault();
			});			
        },
		hide,
		setContent: function(html, events = false) {			
				$( ".popup-content-box" ).html(html);				
				//bind events (only click for now)
				if (events) {
					for(var p in events) {												
						$(`#${p}`).click(events[p]);
					}
				}			
		},
		getFormData: function(html) {
			return formData;
		},			
    };
});
