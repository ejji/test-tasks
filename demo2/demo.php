<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>demo</title>
 </head>
<body>
<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>

<style>
.hide{
	display: none;
}
.clickable{
	cursor: pointer;
}

</style>

<script>
$(document).ready(function(){

function updateFilter(){
	var val = $( "#order_filter option:selected").attr('val');
	$( "#order_table select").removeClass("hide");	
	$( "#order_table select[val!='"+val+"']").addClass("hide");	
	$( "#order_filter").removeClass("hide");
	//console.log("#order_table select[val='"+val+"']");
	
	// calc price
	var base_price = parseInt($( "#order_filter option:selected").attr('add_price'));
	var extra_price 
	extra_price = parseInt($( "#order_table #order_legs:not(:has(.hide)) option:selected").attr('add_price'))+	
	parseInt($( "#order_table #order_color:not(:has(.hide)) option:selected").attr('add_price'));
	
	$( "#price").text(base_price+extra_price);
	
	// fill form data to submit
	$( "#order_table input[name='model']").val($( "#order_filter option:selected").val());
	$( "#order_table input[name='color']").val($( "#order_color option:selected").val()); 
	$( "#order_table input[name='legs']").val($( "#order_legs option:selected").val());
	$( "#order_table input[name='price']").val(base_price+extra_price);
	
}

updateFilter();

$( "#order_table select" ).change(function() {
	updateFilter();
});
	
});
</script>



<?php

if (!function_exists('mysqli_fetch_all')) {
function mysqli_fetch_all(mysqli_result $result) {
	$data = [];
	while ($row = $result->fetch_assoc()) {
		$data[] = $row;
	}
	return $data;
}
}

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "demo2";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM model_list;";
$model_list = $conn->query($sql);
$a_model_list = mysqli_fetch_all($model_list, MYSQLI_ASSOC);

$sql = "SELECT * FROM model_legs_num;";
$model_legs_num = $conn->query($sql);
$a_model_legs_num = mysqli_fetch_all($model_legs_num, MYSQLI_ASSOC);

$sql = "SELECT * FROM model_color;";
$model_color = $conn->query($sql);
$a_model_color = mysqli_fetch_all($model_color, MYSQLI_ASSOC);

//print_r($a_model_legs_num);
//print_r($a_model_color);

?>
<form action="order.php" method="POST">
<table id="order_table">		
	<tr val="1"><th>Тип</th></tr>
	
	<tr><td>
	<select id="order_filter">
		<?php foreach ($a_model_list as $row) {?>
		<option	val="<?php echo $row["model_id"] ?>" add_price="<?php echo $row["model_base_price"]?>">
			<?php echo $row["model_name"] ?>
		</option>
		<?php } ?>
	</select>	
	<input type="hidden" name="model">
	</td></tr>
	
	<tr><th>Количество ножек</th></tr>
	<tr><td>
	<?php foreach ($a_model_list as $row) {?>	
	<select id="order_legs" val="<?php echo $row["model_id"]?>">
	<?php foreach ($a_model_legs_num as $row_legs) {?>
		<?php if ($row_legs["model_id"] == $row["model_id"]) { ?>
			<option add_price="<?php echo $row_legs["model_add_price"]?>"> 					
				<?php echo $row_legs["model_legs_num"] ?>
			</option>				
		<?php } ?>	
	<?php } ?>
	</select>	
	<?php } ?>
	<input type="hidden" name="legs">
	</td></tr>
	
	<tr><th>Цвет</th></tr>
	<tr><td>
	<?php foreach ($a_model_list as $row) {?>	
	<select id="order_color" val="<?php echo $row["model_id"]?>">
	<?php foreach ($a_model_color as $row_color) {?>
		<?php if ($row_color["model_id"] == $row["model_id"]) { ?>
			<option add_price="<?php echo $row_color["model_add_price"]?>"> 					
				<?php echo $row_color["model_color"] ?>
			</option>				
		<?php } ?>	
	<?php } ?>
	</select>	
	<?php } ?>
	<input type="hidden" name="color">
	</td></tr>
	
	
	<tr val="1"><th>Цена</th></tr>	
	<tr><td id="price">
	<input type="hidden" name="price">
	</td></tr>
	
	<tr val="1"><th>Контактные данные</th></tr>	
	<tr><td><input type="text" name="contact">
	</td></tr>
	
	<tr val="1"><th>Контактный телефон</th></tr>	
	<tr><td><input type="text" name="phone">
	</td></tr>
	
	<tr><td>
	<input type="submit">
	</td></tr>
</table>
</form>	
<?php 
	$conn->close();
?>
 </body>
</html>