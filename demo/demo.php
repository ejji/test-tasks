<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>demo</title>
 </head>
<body>
<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>

<style>
.hide{
	display: none;
}
.clickable{
	cursor: pointer;
}

.new-record {
    margin: 20px 0;
    text-align: right;
}
.new-record span {
    background: #A7D6A5;
    padding: 10px;
    border-radius: 20px;
}
.test.container {
    width: 60%;
}
.test {
    width: 100%;
}

.test textarea, .test select {
    width: 100%;
	border: 0px solid;
	background-color: rgba(0, 0, 0, 0);
}

.test tr:nth-child(2n+1) {background: #A7D6A5;}
</style>

<script>
$(document).ready(function(){
    //add new record
	$(".new-record").click(function(){
        dest = $(".template.hide").parent();
		res = $(".template").clone().appendTo(dest);
		res.removeClass("template").removeClass("hide");
		
		$.getJSON( "ajax.php", { do: "insert"} )
		  .done(function( json ) {
			//console.log( json);
			//console.log( json[0]["LAST_INSERT_ID()"]);
			res.find("[accp='product_id']").text(json[0]["LAST_INSERT_ID()"]);
		  })
		  .fail(function( jqxhr, textStatus, error ) {			
			console.log( "Request Failed: " + err );
		});		
    });
	//clear all new fields from help text
	$(document).on('click', '.click-clear', function(){
		//console.log( "Handler for .change() called." );
		$(this).removeClass("click-clear");
		$(this).children().text("");
	});
	
	
	$(document).on('focusout', '.edit-save', function(){		
		//general robust data serialize
		var data = {};	
		var primary = {};
		//procc plain data
		$(this).parents(".data-row").find("[acc]").each(function(){
			data[$(this).attr("acc")]=$(this).children().val();
		});
		//procc data from select
		$(this).parents(".data-row").find("[accs]").each(function(){			
			data[$(this).attr("accs")]=$(this).find(":selected").attr("val");
		});		
		//procc plain data for primary key
		$(this).parents(".data-row").find("[accp]").each(function(){
			primary[$(this).attr("accp")]=$(this).text();
		});					
		//TODO: SHOW UPDATE PROCC GUI ELEMENT
		var send_data = {do : "update"};
		send_data["data"] = JSON.stringify(data);
		send_data["primary"] = JSON.stringify(primary);
		$.getJSON( "ajax.php", send_data )
		  .done(function( json ) {
			console.log( json);
		  })
		  .fail(function( jqxhr, textStatus, error ) {			
			console.log( "Request Failed: " + error );
		});
		
		console.log( "Handler for focusout called." );				
		console.log( send_data );
		console.log( data );					
	});

	//TODO: SHOW QUERY PROCC GUI ELEMENT
	$(document).on('click', '.edit-delete', function(){
		//console.log( "Handler for .change() called." );
		var primary = {};
		$(this).parents(".data-row").find("[accp]").each(function(){
			primary[$(this).attr("accp")]=$(this).text();
		});	
		element = $(this);
		var send_data = {do : "delete"};		
		send_data["primary"] = JSON.stringify(primary);
		$.getJSON( "ajax.php", send_data )
		  .done(function( json ) {
			//console.log( json);
			//console.log( json[0]["LAST_INSERT_ID()"]);
			//res.find("[accp='product_id']").text(json[0]["LAST_INSERT_ID()"]);
			
			if (json.result) {element.parent().remove();} else {alert("Ошибка в базе данных");}
		  })
		.fail(function( jqxhr, textStatus, error ) {			
			console.log( "Request Failed: " + error );
		});
	});	

	
});
</script>



<?php

if (!function_exists('mysqli_fetch_all')) {
function mysqli_fetch_all(mysqli_result $result) {
	$data = [];
	while ($row = $result->fetch_assoc()) {
		$data[] = $row;
	}
	return $data;
}
}

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM products LEFT JOIN category USING(category_id);";
$product_data = $conn->query($sql);

$sql = "SELECT * FROM category;";
$category_data = $conn->query($sql);
$a_category_data = mysqli_fetch_all($category_data, MYSQLI_ASSOC);
//print_r($a_category_data);
?>
<div class="test container">
<div class="new-record clickable"><span>+ добавить</span></div>
<table class="test">
	<tr>
	<td>Артикул</td><td>Описание</td><td>Категория</td><td>Вес</td><td></td>
	</tr>
	<?php while($row = $product_data->fetch_assoc()) { ?>
	<tr class="data-row">		
		<td accp="product_id"><?php echo $row["product_id"] ?></td>	
		<td acc="description" class="edit-save"><textarea><?php echo $row["description"] ?></textarea></td>
		<td accs="category_id" class="edit-save">
		<select>			
			<?php foreach ($a_category_data as $row_category_data) {?>
				<option <?php if ($row_category_data["category_id"] == $row["category_id"]) echo "selected";?> 
				val="<?php echo $row_category_data["category_id"] ?>">
					<?php echo $row_category_data["category_name"] ?>
				</option>					
			<?php } ?>
		</select>
		</td>
		<td acc="weight" class="edit-save"><textarea><?php echo $row["weight"] ?></textarea></td>
		<td class="edit-delete clickable">[x]</td>
	</tr>
	<?php } ?>
	
	<tr class="data-row template hide">
		<td accp="product_id"></td>	
		<td acc="description" class="click-clear edit-save"><textarea>описание</textarea></td>
		<td accs="category_id" class="edit-save">
		<select>			
			<?php foreach ($a_category_data as $row_category_data) {?>
				<option val="<?php echo $row_category_data["category_id"] ?>">
					<?php echo $row_category_data["category_name"] ?>
				</option>					
			<?php } ?>
		</select>
		</td>
		<td acc="weight" class="click-clear edit-save"><textarea>вес</textarea></td>
		<td class="edit-delete clickable">[x]</td>
	</tr>
</table>
<div class="new-record clickable"><span>+ добавить</span></div>
</div>
<?php 
	$conn->close();
?>
 </body>
</html>