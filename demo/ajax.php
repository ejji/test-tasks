<?php

if (!function_exists('mysqli_fetch_all')) {
function mysqli_fetch_all(mysqli_result $result) {
	$data = [];
	while ($row = $result->fetch_assoc()) {
		$data[] = $row;
	}
	return $data;
}
}

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//$sql = "SELECT * FROM products LEFT JOIN category USING(category_id);";
//$product_data = $conn->query($sql);

//$sql = "SELECT * FROM category;";
//$category_data = $conn->query($sql);
//$a_category_data = mysqli_fetch_all ($category_data, MYSQLI_ASSOC);
//print_r($a_category_data);

//INSERT INTO `table` (`primary_key`) VALUES (NULL);
if ($_GET["do"] == "insert"){
$sql = "INSERT INTO products () VALUES();";
$data = $conn->query($sql);

/*
The ID that was generated is maintained in the server on a per-connection basis. This means that the value returned by the function to a given client is the first AUTO_INCREMENT value generated for most recent statement affecting an AUTO_INCREMENT column by that client.
*/

$sql = "SELECT LAST_INSERT_ID();";
$data = $conn->query($sql);

$output = mysqli_fetch_all($data, MYSQLI_ASSOC);
print json_encode($output);

}

if ($_GET["do"] == "update"){
//print_r($_GET["data"]);
$data = json_decode($_GET["data"]);
$primary_data = json_decode($_GET["primary"]);

//var_dump(json_decode($_GET["data"]));
$arg_str="";
foreach ($data as $key=>$value){
	if ($value == "") {$value="NULL";}
	$arg_str .=$key . "='" . $value . "', ";
}
$arg_str = rtrim($arg_str, ", ");

$primary_arg_str="";
foreach ($primary_data as $key=>$value){
	if ($value == "") {$value="NULL";}
	$primary_arg_str .=$key . "=" . $value . ", ";
}
$primary_arg_str = rtrim($primary_arg_str, ", ");

$sql = "UPDATE products SET " . $arg_str . " WHERE " . $primary_arg_str . ";";
$data = $conn->query($sql);

//$output = ["result" => $sql];
$output = ["result" => $data];
print json_encode($output);	
}

if ($_GET["do"] == "delete"){
	
	$primary_data = json_decode($_GET["primary"]);
	$primary_arg_str = "";
	foreach ($primary_data as $key=>$value){
	if ($value == "") {$value="NULL";}
	$primary_arg_str .=$key . "=" . $value . ", ";
	}
	$primary_arg_str = rtrim($primary_arg_str, ", ");
	
	$sql = "DELETE FROM products WHERE " . $primary_arg_str . ";";	
	$data = $conn->query($sql);
	//$output = ["result" => $sql];
	$output = ["result" => $data];
	print json_encode($output);	
}


$conn->close();
?>