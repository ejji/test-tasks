$(document).ready(function () {

    function numberWithSpaces(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    };

    function filterDigits(x) {
        return x.toString().replace(/[^\d]/, '')
    };

    function subscriptionAutoContinuation() {
        // show|hide subsription auto continuation block
        function subscriptionAutoMethods() {
            var selectedPM = $('input[name="payment-method"]:checked');
            if (selectedPM.hasClass('continue')) {
                $('.order-block-2 .extra').show();
            } else {
                $('.order-block-2 .extra').hide();
            }
        };

        subscriptionAutoMethods();

        if ($('input[name="payment-gift"]:checked').length) {
            $('.order-block-2 .extra').hide();
            $('.order-block-3 .extra').hide();
            $('.order-block-3 .extra-2').show();
        } else {
            subscriptionAutoMethods(); //restore product auto continuation block state
            $('.order-block-3 .extra').show();
            $('.order-block-3 .extra-2').hide();
        };
    }

    //hide block in js to make happy js off users
    //it is possible to send form without js
    $('.order-block-2').hide();
    $('.order-block-3').hide();
    $('.order-block-3 .extra-2').hide();
    $('button[type="submit"]').attr('disabled', true); 


    var giftCodePaymentMethod = '#payment-method-7';
    $(giftCodePaymentMethod).change(function () {
        //disable all payment methods exluding gift code
        $('input[name="payment-method"]').addClass('greyed');
        $(this).removeClass('greyed');        

        //disable subscription gift option
        $('input[name="payment-gift"]').attr('disabled', true);
        $('input[name="payment-gift"]').prop('checked', false);
        $('.order-block-1 .extra').hide();
    });

    //disable gift code
    $('input[name="payment-gift"]').change(function () {        
        $(giftCodePaymentMethod).attr('disabled', $(this).prop('checked'));
        subscriptionAutoContinuation();
    });

    $('input[name="payment-method"]').not(giftCodePaymentMethod).change(function () {        
        $('input[name="payment-method"]').removeClass('greyed');

        //enabe subscription gift option
        $('.order-block-1 .extra').show();
        $('input[name="payment-gift"]').attr('disabled', false);        

        //show subscription time block
        $('.order-block-2').show();
        subscriptionAutoContinuation();
    });

    //subsription time selection
    $('input[name="subsription-time"]').change(function () {
        //show price block        
        $('.order-block-3').show();

        //enable submit button
        $('button[type="submit"]').attr('disabled', false);       


        var subscriptionTime =
            $('input[name="subsription-time"]:checked + label .subsription-time-label').text();
        
        //copy subscription time    
        $('.subsription-time').text(subscriptionTime); 

        var subscriptionPrice =
            $('input[name="subsription-time"]:checked + label .subsription-price-label').text();
        
        var extraSubscriptionPrice = parseInt(filterDigits($('.add-price').text()));        

        //make price
        $('.main-price').text(subscriptionPrice);
        $('.result-price').text(
            ' = ' +
            numberWithSpaces(
                parseInt(filterDigits(subscriptionPrice)) + extraSubscriptionPrice
            )
        );
    });

    //show extra price
    $('input[name="payment-discount"]').change(function () {
        $('.price').toggleClass('complex');
    });

});
